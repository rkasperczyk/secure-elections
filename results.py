import election

db = election.database


class Results(db.Model):
    __tablename__ = 'results'

    answer = db.Column(db.String, primary_key=True)
    count_votes = db.Column(db.INTEGER, nullable=False)

    def __init__(self, answer):
        self.answer = answer
        self.count_votes = 0

    def __repr__(self):
        return '|answer: {0}|amount: {1}|'.format(self.answer, self.count_votes)


class Aliases(db.Model):
    __tablename__ = 'aliases'

    alias = db.Column(db.String, primary_key=True)
    voted = db.Column(db.Boolean, nullable=False)

    def __init__(self, alias):
        self.alias = alias
        self.voted = False
