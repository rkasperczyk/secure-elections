import os
import ssl

import requests
from flask import Flask
from flask import render_template
from flask import request
from flask.ext.sslify import SSLify
from flask_sqlalchemy import SQLAlchemy

app_sec_server = Flask(__name__)

app_sec_server.config.update(dict(
    SECRET_KEY='development key',
    SQLALCHEMY_DATABASE_URI='sqlite:///results.db'
))

app_sec_server.config.from_envvar('FLASKR_SETTINGS', silent=True)
global database
database = SQLAlchemy(app_sec_server)
import results
sslify = SSLify(app_sec_server)



@app_sec_server.route('/vote/<user_alias>', methods=['GET', 'POST'])
def vote(user_alias):
    alias = results.Aliases.query.filter(results.Aliases.alias == user_alias).first()
    if alias is not None and alias.voted is False:
        if request.method == 'GET':
            return render_template('voting.html', user_alias=user_alias)
        if request.method == 'POST':
            print user_alias
            requests.put("https://localhost:5000/voted", json={'alias': user_alias}, verify=False)
            if "for" in request.form:
                result = results.Results.query.filter(results.Results.answer == "for").first()
                alias = results.Aliases.query.filter(results.Aliases.alias == user_alias).first()
                alias.voted = True
                result.count_votes += 1
                print result.count_votes
                database.session.commit()
                return "Thank you for voting!"
            elif "against" in request.form:
                result = results.Results.query.filter(results.Results.answer == "against").first()
                alias = results.Aliases.query.filter(results.Aliases.alias == user_alias).first()
                alias.voted = True
                result.count_votes += 1
                database.session.commit()
                return "Thank you for voting!"
            return render_template("voting.html", user_alias=user_alias)
    elif alias is not None and alias.voted is True:
        return "You have already voted!"
    else:
        return "You are not allowed to see that page!"


@app_sec_server.route('/user', methods=['PUT', ])
def user():
    print request.get_json()['alias']
    database.session.add(results.Aliases(request.get_json()['alias']))
    database.session.commit()
    return ""


if __name__ == '__main__':
    ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)  # use TLS to avoid POODLE
    here = os.path.dirname(__file__)
    key = os.path.join(here, 'key2.pem')
    cert = os.path.join(here, 'cert2.pem')
    ctx.load_cert_chain(
        cert,
        key)
    app_sec_server.run(port=5001, ssl_context=ctx)
