import hashlib
import os
import random
import ssl
import string

import bcrypt as bcrypt
import requests
from OpenSSL import SSL
from flask import Flask, request, redirect, render_template, flash
from flask_login import current_user
from flask_login import LoginManager, login_required, login_user, logout_user
from flask_sqlalchemy import SQLAlchemy
from flask_sslify import SSLify

app = Flask(__name__)
sslify = SSLify(app)

app.config.update(dict(
    SECRET_KEY='development key',
    SQLALCHEMY_DATABASE_URI='sqlite:///users.db'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)
global db
db = SQLAlchemy(app)

from users import *

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
global_salt = "1a72d5209d3789f6e4daaa7896fe525ad7ba32ee6deaffa6741668df7b12dbb4"


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter(User.pesel == str(user_id)).first()


@app.route('/')
def starting_page():
    # requests.put("http://localhost:5001/putTest", json={'value': 'Zara'})
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        user = User.query.filter(User.username == request.form.get("username")).first()
        if user is not None and user.password == hashlib.sha256(
                                global_salt + request.form.get("password") + user.salt).hexdigest():
            login_user(user)
            return redirect("/content")
        else:
            error = "Invalid login data!!!"
    return render_template('login.html', error=error)


@app.route('/sign-up', methods=['GET', 'POST'])
def sign_up():
    error = None
    if request.method == 'POST':
        user = User.query.filter(User.username == request.form.get("username")).first()
        if user is None:
            user_salt = bcrypt.gensalt()
            char_set = string.ascii_uppercase + string.digits + string.ascii_lowercase
            user_alias = ''.join(random.sample(char_set * 64, 64));

            db.session.add(User(request.form.get('username'), request.form.get('name'), request.form.get('surname'),
                                request.form.get('pesel'), request.form.get('email'),
                                hashlib.sha256(global_salt + request.form.get('password') + user_salt).hexdigest(),
                                user_salt, user_alias))
            db.session.commit()
            requests.put("https://localhost:5001/user", json={'alias': user_alias}, verify=False)
            return redirect("/")
        elif user is not None:
            error = "User with that username has already signed up."
            return render_template('sign-up.html', error=error)

    return render_template('sign-up.html', error=error)


@app.route('/content')
@login_required
def content():
    user_alias = current_user.alias;
    return render_template("elections.html", user_alias=user_alias)


@app.route('/voted', methods=['PUT', ])
def voted():
    print request.get_json()['alias']
    user = User.query.filter(User.alias == request.get_json()['alias']).first()
    user.voted = True
    print user
    db.session.commit()
    return ""


@app.route('/logout')
def logout():
    logout_user()
    flash("You were logged out")
    return redirect("/login")


if __name__ == '__main__':
    ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)  # use TLS to avoid POODLE

    here = os.path.dirname(__file__)
    key = os.path.join(here, 'key.pem')
    cert = os.path.join(here, 'cert.pem')

    ctx.load_cert_chain(
        cert,
        key)
    app.run(ssl_context=ctx, debug=False)
