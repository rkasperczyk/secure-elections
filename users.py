import SecureElection

db = SecureElection.db


class User(db.Model):
    __tablename__ = 'users'

    username = db.Column(db.String, nullable=False)
    name = db.Column(db.String, nullable=False)
    surname = db.Column(db.String, nullable=False)
    pesel = db.Column(db.String, primary_key=True)
    email = db.Column(db.String, nullable=False)
    password = db.Column(db.String, nullable=False)
    salt = db.Column(db.String, nullable=False)
    is_authenticated = db.Column(db.Boolean, nullable=False)
    alias = db.Column(db.String, nullable=False)
    voted = db.Column(db.String, nullable=False)

    def __init__(self, username, surname, name, pesel, email, password, salt, alias):
        self.username = username
        self.name = name
        self.surname = surname
        self.pesel = pesel
        self.email = email
        self.password = password
        self.salt = salt
        self.is_authenticated = False
        self.alias = alias
        self.voted = False

    def __repr__(self):
        return '<pesel:{}'.format(self.pesel)

    def is_anonymous(self):
        return False

    def is_active(self):
        return True

    def get_id(self):
        return unicode(self.pesel)

    def is_authenticated(self):
        return self.is_authenticated
