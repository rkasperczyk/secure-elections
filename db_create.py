from SecureElection import db
from election import database
from results import Results

db.create_all()
database.create_all()

database.session.add(Results("for"))
database.session.add(Results("against"))

database.session.commit()
